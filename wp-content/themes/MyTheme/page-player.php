<?php 
session_start();
    //header section 
    get_header();
    include("DBConnection.php");
    $connection = new mysqli("localhost", $username, $password,$database);

	/*<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
		<script>
		function showEdit(editableObj) {
			$(editableObj).css("background","#FFF");
		} 
		
		function saveToDatabase(editableObj,column,id) {
			$(editableObj).css("background","#FFF url(loaderIcon.gif) no-repeat right");
			$.ajax({
				url: "saveedit.php",
				type: "POST",
				data:'column='+column+'&editval='+editableObj.innerHTML+'&id='+id,
				success: function(data){
					$(editableObj).css("background","#FDFDFD");
				}        
		   });
		}
		</script>*/
?>
<div class="wrapper">
<?php 
    $name=$_REQUEST['PlayerName'];
    
    $query2="SELECT p.PlayerID, p.PlayerName, p.BirthDate, p.biography, p.tshirtNumber, t.TeamName, tp.Type
    FROM players p, team t,teamtype tp
    where p.TeamID=t.TeamID and t.TeamTypeID=tp.TeamTypeID and p.PlayerName= ? ;";
    
    if($stmt = $connection->prepare($query2)){
        $stmt->bind_param('s',$name);
        $stmt->execute();
        $stmt->bind_result($PlayerID,$PlayerName,$BirthDate,$biography,$tshirtNumber,$TeamName,$Type);
    
    
    echo"<b><center>Player Informations</center></b><br>
    <table class='table-bordered table custom-table'>
    <thead class='thead-light'>
    <tr>
        <th> ID</th>
        <th> Name</th>
        <th> BirthDate</th>
        <th> Biography</th>
        <th> T-shirt Number</th>    
        <th> Team Name</th>
        <th> Team Type</th>
        <th> Edit Info</th>
    
    </tr>
    </thead>";
        $num=0;
    while ($stmt->fetch()){
        echo"<tr>
        <td contenteditable='true' id='playerid'".$num.">"; print $PlayerID; echo"</td>
            
        <td contenteditable='true' id=playername'".$num.">"; print $PlayerName; echo"</td> 
        <td contenteditable='true' id='BD'".$num.">"; print $BirthDate; echo"</td> 
        
        <td contenteditable='true' id='bio'".$num.">"; print $biography; echo"</td> 
        <td contenteditable='true' id='Tnum'".$num.">"; print $tshirtNumber; echo"</td> 
        <td contenteditable='true' id='teamname'".$num.">"; print $TeamName; echo"</td>
        <td contenteditable='true' id='type'".$num.">"; print $Type; echo"</td> 
        <td> <input type='button' class='btn btn-defult' value='EDIT' id='edit_button'+".$num." onclick='edit_row(".$num.");'/>
            <input type='button' class='btn btn-defult' value='SAVE' id='save_button'+".$num." onclick='save_row(".$num.")'/></td>
        </tr>";
        $num++;
    }
    echo"</table>";
    $stmt->free_result();
    $stmt->close();
    
    } else die("Failed to prepare!");

    $connection->close();
    ?>
       
</div>
<?php
    //footer section
    get_footer();
?>