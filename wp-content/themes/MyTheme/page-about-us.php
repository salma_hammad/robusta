<?php 
    //header section 
    get_header();
?>
<div class="wrapper">
<?php  
//print the posts (title and content)
    if(have_posts()){
        while(have_posts()){
            the_post();
            ?>
         <div class="single_post">
            <h1 class="post_title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
            <span> 
                <?php
                //to display the category f each post
                $category = get_the_category();
                $seperator = ',';
                $output = '';
                if($category){
                    foreach( $category as $categories){
                        $output .= '<a class="categories"  href="'.get_category_link($categories->term_id). '">' .  $categories->cat_name. '</a>' . $seperator;
                    }
                    echo $output;
                }
                ?>  
            </span>
             <p class="content_post"> <?php the_content();?></p>
        </div> <!-- main single post container -->
            <?php
        }//while ends
    }//if ends
?>
</div>
<?php
    //footer section
    get_footer();
?>