<?php 
session_start();
    //header section 
    get_header();
    include("DBConnection.php");
    $connection = new mysqli("localhost", $username, $password,$database);
?>
<div class="wrapper">
<?php 
    $name=$_REQUEST['TeamName'];
    
    $query2="SELECT p.PlayerName FROM players p, team t
                where p.TeamID=t.TeamID and t.TeamName= ?;";
    
    if($stmt = $connection->prepare($query2)){
        $stmt->bind_param('s',$name);
        $stmt->execute();
        $stmt->bind_result($res );
    
        echo"<b><center>Our Team members</center></b><br>
        <table class='table-bordered table custom-table'>
        <thead class='thead-light'>
        <tr>
            <th>Player Name</th>
        </tr>
        </thead>";
        
        while ($stmt->fetch()){
            echo"<tr><td>"; 
            echo('<a href="player?PlayerName='.$res.'">'.$res.'</a>');
            echo"</td></tr>";
        }
        echo "</table>";
        $stmt->free_result();
        $stmt->close();
 
    } else die("Failed to prepare!");

    $connection->close();
?>
</div>
<?php
    //footer section
    get_footer();
?>