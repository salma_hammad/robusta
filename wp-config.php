<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'football_club');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';%G*UX!N5HZ9EM%Ej! nyF>>9DVC]B8TkJU>3Z#DN/Njsv/R[VzXAD/l,R|<$#[$');
define('SECURE_AUTH_KEY',  '`uY/cI@Nr4AjnSsFHT5lp3y#T#gA!)<7@sCU|Pnt^hCR,`DdT93|]Xj_MZ1WEiWk');
define('LOGGED_IN_KEY',    'F)1k;[lHR9dCgLdP!WODA9Jce<0j@+iVP}NR8UmO# c7A?Q}<V^rZQ<rjDzGVOyP');
define('NONCE_KEY',        'gbowYR$X(@}%<a00`Ye%XMLNb8B kB/APio9; z{_Y*wRPvc{43?E~kCv>%RLc2G');
define('AUTH_SALT',        '$JT(R6aMusnB,(3nUM`JT]*$khiqnq!m6D`t:.c6JvtG_]s!{&;=%bF<6cN_%k(n');
define('SECURE_AUTH_SALT', 'G,tK$h*pHm&W?/b3jv&z*J|g:Vjx;)U0+Wp~lT qu0|$@>?O_ ri@GdQ67R,n:lt');
define('LOGGED_IN_SALT',   'J6>lvc+u^@hJ0s?5O+v>Yd_DA@L]D{V(p_S@p -v;u=NN_MIddWhD}%=$X:-#{To');
define('NONCE_SALT',       '@fk%8Uz]ha/f2W(R#[1=>Dk_I3wP{+BzO^6(NHG.*R6);6l${~e| pI_x,_$Cs9L');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
