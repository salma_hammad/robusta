<?php
//////////////////////////
    //CSS Registration
//////////////////////////
        //custom function to add style sheet
    function CSS_Registration(){
        //wordpress defualt function - has 2 parameters
        //parameter 1 is ID , parameter 2 is a function concatinated with the style file path
        wp_enqueue_style('Custom_CSS' , get_template_directory_uri() . '/style.css');
        wp_enqueue_style('Bootstrap_CSS' , get_template_directory_uri() . '/bootstrap/css/bootstrap.css');
        wp_enqueue_style('Bootstrap_CSS.min' , get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
        wp_enqueue_style('Bootstrap_CSS-grid' , get_template_directory_uri() . '/bootstrap/css/bootstrap-grid.css');
    }
    //wordpress function with 2 parameters 'actions are hooks that launches at a specific point of execution'
    //parameter1 is name of action the function is hooked to, parameter2 is name of function to call
    add_action('wp_enqueue_scripts','CSS_Registration');

//////////////////////////
    //javaScript Registration
//////////////////////////
    function JS_Registration(){
        wp_enqueue_script('Jquery_JS', get_template_directory_uri() . '/js/jquery.js');
        wp_enqueue_script('Custom_JS', get_template_directory_uri() . '/js/custom.js');
        wp_enqueue_script('Custom_Table_JS', get_template_directory_uri() . '/js/Table.js');        
    }    
    do_action('wp_enqueue_scripts','JS_Registration');
    

//////////////////////////
    //Register nav menus
//////////////////////////

    register_nav_menus( array( 
        'primary'=>__('Primary Menu')
    ));
  
//////////////////////////
    //Bootstrap Registeration
//////////////////////////

function wpbootstrap_scripts_with_jquery()
{
    // Register the script like this for a theme:
    wp_enqueue_style( 'bootstrapstyle', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
    wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array(), true );
}

add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

//////////////////////////
    //Featured image section
//////////////////////////
    add_theme_support('post-thumbnails');

?>